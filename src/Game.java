public class Game {
    /**
     * Game status
     */
    private GameStatus gameStatus;
    // Add commit in master
    /**
     * Branch commit
     */
    
    private int myBranchCommit;
    /**
     * Game's field for alive player
     */
    private Field aliveGameField;

    /**
     * Game's field for AI player
     */
    private Field AIGameField;

    /**
     * Alive player
     */
    private Player player;

    /**
     * AI Player
     */
    private PlayerAI playerAI;

    public Game(int fieldSize, String playerName) {
        if(fieldSize < 10){
            throw new GameCreationException("GameField can't be smaller than 10x10 cells!");
        }

        player = new Player();
        player.setName(playerName);
        player.setGame(this);

        playerAI = new PlayerAI();
        playerAI.setName("SekaBot");
        playerAI.setGame(this);

        aliveGameField = new Field(fieldSize);
        AIGameField= new Field(fieldSize);
    }

    public Player getPlayer() { return player; }
    public PlayerAI getAIPlayer() { return playerAI; }

    public int getFieldSize() { return getAIGameField().getFieldSize(); }

    public Field getAliveGameField() { return aliveGameField; }
    public Field getAIGameField() { return AIGameField; }
    // Add second commit in master
    public void init() { gameStatus = GameStatus.GAME_IS_ON; }

    public void abort() { gameStatus = GameStatus.GAME_ABORTED; }

    public GameStatus getGameStatus() { return gameStatus; }

    /**
     * ������� ���������� ����
     * @return �������� ����������
     */
    public Player getWinner() {
        Player winner = playerAI;
        if(player.getIfActive())
            winner = player;

        return winner;
    }

    public GameStatus playerMakesMove(Cell cell) {
        if (player.getIfActive()) {
            if (player.makeMove(cell)) {

                if(AIGameField.getUnbrokenShipParts() == 0){
                    gameStatus = GameStatus.WINNER_FOUND;

                    return gameStatus;
                }
            }
            else {
                player.setActive(false);
                playerAI.setActive(true);
                // ��� ���� ��� ������������ �����
                while (playerAI.makeMove(cell));

                player.setActive(true);
                playerAI.setActive(false);

                // Check git fetch
                if(aliveGameField.getUnbrokenShipParts() == 0){
                    gameStatus = GameStatus.WINNER_FOUND;

                    player.setActive(false);
                    playerAI.setActive(true);

                    return gameStatus;
                }
            }
        }
        //Add second master
        System.out.println("Some message");
        return gameStatus;
    }
}