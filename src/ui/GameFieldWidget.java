import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Map;

public class GameFieldWidget extends JPanel {
    private final GameWidget owner;
    private Field field;
    private ArrayList<CellWidget> cellWidgets = new ArrayList<>();
	// master edit
    public GameFieldWidget(GameWidget owner, Boolean isAlivePlayer, GameWidget.PlayerActionObserver observer) {
        System.out.println("HELLO");
        this.owner = owner;

        //field auto setter
        if(isAlivePlayer) this.field = owner.getGame().getAliveGameField();
        else this.field = owner.getGame().getAIGameField();

        int fieldSize = owner.getGame().getFieldSize();

        //field with cells creation
        setLayout(new GridLayout(fieldSize, fieldSize, 4, 4));
        for(int i = 0; i < fieldSize; i++)
            for (int j = 0; j < fieldSize; j++) {
                CellWidget cell = new CellWidget(field.getCellByPoint(new Point(i,j)), Styles.SEA_COLOR, Styles.HOVERED_SEA_BUTTON);
                //remove this comment to see both fields filling
                //setIcon(cell);
                if(isAlivePlayer) {
                    cell = new CellWidget(field.getCellByPoint(new Point(i,j)), Styles.SEA_COLOR);
                    setIcon(cell);
                }


                cell.setListener(observer);

                add(cell);
                this.cellWidgets.add(cell);
            }
        System.out.println("END");
    }

    public void setIcon(CellWidget cellWidget) {
        System.out.println("SET ICON");

        switch (cellWidget.getCell().getUnitType()) {
            case SHIP_PART -> cellWidget.setIcon(new ImageIcon(this.getClass().getResource("shipPart.png")));
            case BROKEN_SHIP_PART -> cellWidget.setIcon(new ImageIcon(this.getClass().getResource("boomMark.png")));
            case NAVAL_MINE -> cellWidget.setIcon(new ImageIcon(this.getClass().getResource("navalMineMark.png")));
            case ISLAND -> cellWidget.setIcon(new ImageIcon(this.getClass().getResource("islandMark.png")));
            case LIGHTHOUSE -> cellWidget.setIcon(new ImageIcon(this.getClass().getResource("deadLighthouseMark.png")));
        }

        System.out.println("END");
    }

    public void setDisabledIcon(CellWidget cellWidget) {
        switch (cellWidget.getCell().getUnitType()) {
            case SHIP_PART -> cellWidget.setDisabledIcon(new ImageIcon(this.getClass().getResource("shipPart.png")));
            case BROKEN_SHIP_PART -> cellWidget.setIcon(new ImageIcon(this.getClass().getResource("boomMark.png")));
            case NAVAL_MINE -> cellWidget.setDisabledIcon(new ImageIcon(this.getClass().getResource("navalMineMark.png")));
            case ISLAND -> cellWidget.setDisabledIcon(new ImageIcon(this.getClass().getResource("islandMark.png")));
            case LIGHTHOUSE -> cellWidget.setDisabledIcon(new ImageIcon(this.getClass().getResource("deadLighthouseMark.png")));
        }
    }

    public ArrayList<CellWidget> getCellWidgets() { return cellWidgets; }

    public void hitRepaint(Cell cell) {
        // hitRepaint comment
        if (cell.getUnitType() == UnitType.BROKEN_SHIP_PART) {
            Cell finalCell1 = cell;
            cellWidgets.forEach(s -> {
                if(s.getCell() == finalCell1)
                    setIcon(s);
            });

            if(ShipPart.isShipWrecked(cell)) {
                Map<Direction, Cell> neighbours = cell.getNeighborCells();
                Direction dir = Direction.NORTH;
                for (Map.Entry<Direction, Cell> entry : neighbours.entrySet()) {
                    if (entry.getValue().getUnitType() == UnitType.SHIP_PART || entry.getValue().getUnitType() == UnitType.BROKEN_SHIP_PART) {
                        dir = entry.getKey();
                    }
                }

                while (cell.getNeighborCell(dir) != null){
                    if(cell.getNeighborCell(dir).getUnitType() == UnitType.SHIP_PART || cell.getNeighborCell(dir).getUnitType() == UnitType.BROKEN_SHIP_PART)
                        cell = cell.getNeighborCell(dir);
                    else
                        break;
                }


                for(int i=0; i<4; i++) {
                    if(cell.getUnitType() == UnitType.BROKEN_SHIP_PART) {
                        neighbours = cell.getNeighborCells();
                        for (Map.Entry<Direction, Cell> entry : neighbours.entrySet()) {
                            if (entry.getValue().getUnitType() != UnitType.BROKEN_SHIP_PART) {
                                cellWidgets.forEach(s -> {
                                    if(s.getCell() == entry.getValue()) {
                                        s.setIcon(new ImageIcon(this.getClass().getResource("hitMark.png")));
                                        s.getCell().hit();
                                    }
                                });
                            }
                        }

                        if (cell.getNeighborCell(dir.getOppositeDirection()) != null && cell.getNeighborCell(dir.getOppositeDirection()).getUnitType() == UnitType.BROKEN_SHIP_PART) {
                            cell = cell.getNeighborCell(dir.getOppositeDirection());
                        }
                        else
                            break;
                    }
                }
            }
        }
        else if (cell.getUnitType() == UnitType.NAVAL_MINE) {
            Cell finalCell = cell;

            ArrayList<Cell> toBeRepainted = NavalMine.getAllExplodedNeighbours(cell);

            cellWidgets.forEach(s -> {
                if(s.getCell() == finalCell) {
                    s.setIcon(new ImageIcon(this.getClass().getResource("explodedNavalMineMark.png")));
                }
                else if(toBeRepainted.contains(s.getCell())) {
                    setIcon(s);
                    if(s.getCell().getUnitType() == UnitType.NONE)
                        s.setIcon(new ImageIcon(this.getClass().getResource("hitMark.png")));
                    else if(s.getCell().getUnitType() == UnitType.ISLAND)
                        s.setIcon(new ImageIcon(this.getClass().getResource("deadIslandMark.png")));
                    else if(s.getCell().getUnitType() == UnitType.LIGHTHOUSE && s.getCell().isHitted())
                        s.setIcon(new ImageIcon(this.getClass().getResource("explodedLighthouseMark.png")));
                }
            });
        }
        else if (cell.getUnitType() == UnitType.ISLAND) {
            Cell finalCell = cell;
            cellWidgets.forEach(s -> {
                if(s.getCell() == finalCell) {
                    s.setIcon(new ImageIcon(this.getClass().getResource("deadIslandMark.png")));
                }
            });
        }
        else if (cell.getUnitType() == UnitType.LIGHTHOUSE) {
            Cell finalCell = cell;
            ArrayList<Cell> toBeRepainted = Lighthouse.getAllLightedCells(cell, 3);

            cellWidgets.forEach(s -> {
                if(s.getCell() == finalCell) {
                    s.setIcon(new ImageIcon(this.getClass().getResource("lighthouseMark.png")));
                }
                else if(toBeRepainted.contains(s.getCell())) {
                    setIcon(s);
                    if(s.getCell().getUnitType() == UnitType.NONE)
                        s.setIcon(new ImageIcon(this.getClass().getResource("hitMark.png")));
                    else if(s.getCell().getUnitType() == UnitType.ISLAND)
                        s.setIcon(new ImageIcon(this.getClass().getResource("islandMark.png")));
                }
            });
        }
        else if (cell.getUnitType() == UnitType.NONE){
            Cell finalCell = cell;
            cellWidgets.forEach(s -> {
                if(s.getCell() == finalCell) {
                    s.setIcon(new ImageIcon(this.getClass().getResource("hitMark.png")));
                }
            });
        }

        repaint();
        // end of hitRepaint commit
    }
}
