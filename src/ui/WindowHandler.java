import javax.swing.*;
import java.awt.*;

public class WindowHandler extends JFrame {
    private final GameMenuWidgetModal gameMenuModal = new GameMenuWidgetModal(this);
    private final GameWidget gameWidget = new GameWidget(this);

    public WindowHandler(){
        setTitle("SekaShips");
        //setSize(1160, 700);
        setSize(1160, 700);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new CardLayout());
        contentPane.setBackground(Styles.PRIMARY_BACKGROUND_COLOR);

        Image icon = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("hitMark.png"));
        setIconImage(icon);

        gameMenuModal.setVisible(true);
        contentPane.add(this.gameWidget, "Game");

        setContentPane(contentPane);
        pack();
        setLocationByPlatform(true);
        setVisible(true);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        setVisible(true);
    }

    public void runGame(int fieldSize, String playerName) {
        gameWidget.setupGame(new Game(fieldSize, playerName));
        gameWidget.setVisible(true);

        JLabel AIRulesHeader = CustomJLabel.createCustomLabel("AI makes his turn right after you!", Styles.PRIMARY_FONT);
        CustomModal AIRulesModal = new CustomModal(this, "AI Rules!", AIRulesHeader, 335,150);
        CustomJButton okButt = new CustomJButton("OK", Styles.REGULAR_BUTTON, Styles.REGULAR_HOVERED_BUTTON);
        okButt.addActionListener(w -> AIRulesModal.setVisible(false));
        AIRulesModal.addButton(okButt);

        AIRulesModal.setVisible(true);
    }
}