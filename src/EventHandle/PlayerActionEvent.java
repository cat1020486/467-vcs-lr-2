import java.util.EventObject;

public class PlayerActionEvent extends EventObject {
    private Cell cell;

    public PlayerActionEvent(Object object) {
        super(object);
    }

    public void setCell(Cell cell) { this.cell = cell; }

    public Cell getCell() { return cell; }
}
